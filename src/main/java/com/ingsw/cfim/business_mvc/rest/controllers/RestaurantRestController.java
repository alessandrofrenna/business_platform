package com.ingsw.cfim.business_mvc.rest.controllers;

import com.ingsw.cfim.business_mvc.products.models.Product;
import com.ingsw.cfim.business_mvc.products.repositories.ProductRepository;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import com.ingsw.cfim.business_mvc.restaurants.repositories.RestaurantRepository;
import com.ingsw.cfim.business_mvc.restaurants.responses.ProductsCategorized;
import com.ingsw.cfim.business_mvc.restaurants.responses.RestaurantInfo;
import com.ingsw.cfim.business_mvc.restaurants.responses.RestaurantInfoExtended;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/restaurants")
public class RestaurantRestController {
  @Autowired
  private RestaurantRepository restaurantRepository;

  @Autowired
  private ProductRepository productRepository;

  @GetMapping("/{id}")
  public ResponseEntity<RestaurantInfoExtended> getRestaurant(@PathVariable ObjectId id) {
    Restaurant restaurant = restaurantRepository.findById(id).orElse(null);

    if (restaurant == null) {
      return ResponseEntity.status(404).body(null);
    }

    return ResponseEntity.ok(new RestaurantInfoExtended(restaurant));
  }

  @GetMapping("/{id}/products")
  public ResponseEntity<ProductsCategorized> getRestaurantAndProducts(@PathVariable ObjectId id) {
    Restaurant restaurant = restaurantRepository.findById(id).orElse(null);
    List<Product> products = productRepository.findByRestaurantId(restaurant.getId()).orElse(null);

    if (products == null) {
      return ResponseEntity.status(404).body(null);
    }

    return ResponseEntity.ok(new ProductsCategorized(products));
  }

  @GetMapping("/")
  public ResponseEntity<List<RestaurantInfo>> getAllRestaurants(
      @RequestParam(value = "lat", required = false) Double latitude,
      @RequestParam(value = "lng", required = false) Double longitude,
      @RequestParam(value = "dist", required = false) Double distance) {
    List<Restaurant> results = null;

    if (latitude == null && longitude == null) {
      results = this.restaurantRepository.findAll();
    }

    GeoJsonPoint point = new GeoJsonPoint(longitude, latitude);

    if (distance == null) {
      results = this.restaurantRepository.findByAddressNear(point).orElse(null);
    }

    results = this.restaurantRepository.findByAddressNear(point, distance).orElse(null);

    List<RestaurantInfo> infos = results.stream().map((Restaurant r) -> new RestaurantInfo(r))
        .collect(Collectors.toList());

    return ResponseEntity.ok(infos);
  }
}
