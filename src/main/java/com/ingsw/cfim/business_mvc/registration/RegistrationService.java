package com.ingsw.cfim.business_mvc.registration;

import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.common.repositories.RoleRepository;
import com.ingsw.cfim.business_mvc.common.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService implements IRegistrationService {
  @Autowired
  private PasswordEncoder passwordService;

  @Autowired
  private UserRepository userRepo;

  @Autowired
  private RoleRepository roleRepo;

  @Override
  public User register(User user) {
    if(this.userRepo.findUserByEmail(user.getEmail()) == null) {
      user.setPassword(
          this.hashPassword(user.getPassword())
      );

      user.setRoles(this.roleRepo.findByRole("restaurant_owner"));
      return this.userRepo.save(user);
    } else {
      throw new DuplicateKeyException("User with the same email already exists");
    }
  }

  @Override
  public String hashPassword(String plainText) {
    return this.passwordService.encode(plainText);
  }
}
