package com.ingsw.cfim.business_mvc.registration;

import com.ingsw.cfim.business_mvc.profile.builders.OwnerBuilder;
import com.ingsw.cfim.business_mvc.profile.models.OwnerProfile;
import com.ingsw.cfim.business_mvc.registration.forms.RegistrationForm;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Controller
public class RegistrationController {

  IRegistrationService registrationService;

  public RegistrationController(IRegistrationService svc) {
    this.registrationService = svc;
  }

  @GetMapping("/registration")
  public String showRegistrationForm(RegistrationForm registrationForm, Model model) {
    model.addAttribute("registration_error", model.getAttribute("registration_error"));
    return "pages/registration/index";
  }

  @PostMapping("/registration")
  public String saveRegistrationFormDataToSession(@Valid RegistrationForm registrationForm, BindingResult bindingResult, HttpSession session) {
    if(bindingResult.hasErrors()) {
      return "pages/registration/index";
    }

    session.setAttribute("UserForm", registrationForm);
    return "redirect:registration/profile";
  }

  @GetMapping("/registration/profile")
  public String showProfileForm(OwnerProfile ownerProfile, HttpSession session, Model model) {
    if(session.getAttribute("UserForm") == null) {
      throw new  ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    model.addAttribute("add", true);
    return "pages/profile/add-edit";
  }

  @PostMapping("/registration/complete")
  public String addOwnerWithProfileData(@Valid OwnerProfile ownerProfile, BindingResult bindingResult, HttpSession session, RedirectAttributes redirect, Model model) {
    if(bindingResult.hasErrors()) {
      model.addAttribute("add", true);
      return "pages/profile/add-edit";
    }

    try {
      RegistrationForm userForm = (RegistrationForm) session.getAttribute("UserForm");

      OwnerBuilder.init(this.registrationService)
          .user(userForm)
          .profile(ownerProfile)
          .build();

      redirect.addFlashAttribute("login_success", "User created, login with created credentials");

      return "redirect:/login";
    } catch(DuplicateKeyException error) {
      session.removeAttribute("UserForm");
      redirect.addFlashAttribute("registration_error", "User already exists");
      return "redirect:/registration";
    }
  }

}
