package com.ingsw.cfim.business_mvc.restaurants.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ingsw.cfim.business_mvc.products.forms.ProductForm;
import com.ingsw.cfim.business_mvc.products.models.Product;
import com.ingsw.cfim.business_mvc.restaurants.forms.RestaurantForm;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import com.ingsw.cfim.business_mvc.restaurants.models.TimeTable;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Set;

public interface IRestaurantsService {
  Restaurant getRestaurant(ObjectId id);
  List<Restaurant> getAllRestaurants();
  void createRestaurant(RestaurantForm input) throws JsonProcessingException;
  void updateRestaurant(ObjectId restaurantId, RestaurantForm input) throws JsonProcessingException;
  void deleteRestaurant(ObjectId restaurantId);

  List<Product> getRestaurantProducts(ObjectId restaurantId);
  void addProduct(ObjectId restaurantId, ProductForm input);
  void updateProduct(ObjectId restaurantId, ObjectId productId, ProductForm input);
  void deleteProductFromRestaurant(ObjectId productId, ObjectId restaurantId);

  void updateWorkingTimeScheduleForRestaurant(ObjectId restaurantId, TimeTable schedule);

  Restaurant parseRestaurantFormToRestaurant(RestaurantForm form) throws JsonProcessingException;
  RestaurantForm parseRestaurantToRestaurantForm(ObjectId restaurantId);

  ProductForm parseProductToProductForm(ObjectId productId, ObjectId restaurantId);

  Boolean hasProductsOfGivenCategories(Restaurant restaurant, Set<String> categories);
}
