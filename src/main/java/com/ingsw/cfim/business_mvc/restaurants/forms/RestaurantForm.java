package com.ingsw.cfim.business_mvc.restaurants.forms;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class RestaurantForm {
  @NotBlank(message = "Name must not be empty and a valid string")
  String name;

  @NotBlank(message = "Cuisine must not be empty and a valid string")
  String cuisine;

  @NotBlank(message = "Type must not be empty and a valid string")
  String type;

  @NotNull(message="Categories must be a list of comma separated strings")
  Set<String> foodCategories;

  @NotBlank(message = "Description must not be empty and a valid string")
  String description;

  @NotBlank(message = "Phone number must not be empty and a valid string")
  @Size(min = 7, max = 15, message = "Phone number length must be between 7 and 15 digits")
  String phoneNumber;

  @NotBlank(message = "Address must not be empty and a valid string")
  String address;

  @NotBlank(message = "Vat ID must not be empty and a valid string")
  @Size(min=12, max = 12, message = "Vat ID must have a length of 12 digits")
  String vatNumber;

  public void setAddress(String address) {
    String[] addressArray = address.split(" ");
    this.address = String.join("+", addressArray);
  }
}
