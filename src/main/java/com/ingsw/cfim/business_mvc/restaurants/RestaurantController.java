package com.ingsw.cfim.business_mvc.restaurants;

import com.ingsw.cfim.business_mvc.common.exceptions.ResourceNotFoundException;
import com.ingsw.cfim.business_mvc.restaurants.responses.RestaurantInfo;
import com.ingsw.cfim.business_mvc.restaurants.forms.RestaurantForm;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import com.ingsw.cfim.business_mvc.restaurants.models.TimeTable;
import com.ingsw.cfim.business_mvc.restaurants.services.IRestaurantsService;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Slf4j
@RequestMapping("/restaurants")
public class RestaurantController {

  private IRestaurantsService restaurantsService;

  public RestaurantController(IRestaurantsService svc) {
    this.restaurantsService = svc;
  }

  @GetMapping("/")
  @ResponseBody
  public List<RestaurantInfo> getUserRestaurants() {
    return restaurantsService.getAllRestaurants()
        .stream()
        .map(RestaurantInfo::new)
        .collect(Collectors.toList());
  }

  @GetMapping("/{id}")
  public String showRestaurantWithGivenId(@PathVariable("id") ObjectId id, TimeTable timeTable, Model model) {
    Restaurant found = restaurantsService.getRestaurant(id);

    TimeTable schedule = found.getWeeklyOpeningSchedule();

    if(schedule != null) {
      timeTable = schedule;
    }

    System.out.println(timeTable);

    model.addAttribute("restaurant", restaurantsService.getRestaurant(id));
    model.addAttribute("timeTable", timeTable);
    return "pages/restaurants/index";
  }

  @PostMapping("/{id}/timetable")
  public String updateTimeTable(@PathVariable("id") ObjectId id, TimeTable timeTable, BindingResult results, Model model) {
    if(results.hasErrors()) {
      System.out.println(results.getAllErrors());
    }

    String path = String.format("redirect:/restaurants/%s", id.toString());

    try {
      restaurantsService.updateWorkingTimeScheduleForRestaurant(id, timeTable);
      return path;
    } catch(ResourceNotFoundException exception) {
      log.error("Restaurant with id {} not found, cannot update schedule", id.toString());
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/create")
  public String showCreateForm(RestaurantForm restaurantForm, Model model) {
    model.addAttribute("add", true);
    return "pages/restaurants/add-edit";
  }

  @PostMapping("/create")
  public String createRestaurant(@Valid RestaurantForm restaurantForm, BindingResult bindingResult, Model model, RedirectAttributes redirect) {
    if(bindingResult.hasErrors()) {
      model.addAttribute("add", true);
      return "pages/restaurants/add-edit";
    }

    try {
      restaurantsService.createRestaurant(restaurantForm);
    } catch (DuplicateKeyException exception) {
      log.error("Duplicate entries for restaurant with name {}", restaurantForm.getName());
      redirect.addFlashAttribute("creation_error", "Errore di creazione, attività già esistente");
    } catch (Exception exception) {
      log.error("Error creating restaurant {}: {}", restaurantForm.getName(), exception.getMessage());
      redirect.addFlashAttribute("creation_error", "Errore di creazione, origine sconosciuta");
    }
    return "redirect:/profile";
  }

  @GetMapping("/{id}/update")
  public String showUpdateForm(@PathVariable("id") ObjectId id, RestaurantForm restaurantForm, Model model) {
    model.addAttribute("add", false);
    model.addAttribute("restaurantForm", restaurantsService.parseRestaurantToRestaurantForm(id));
    model.addAttribute("id", id);
    return "pages/restaurants/add-edit";
  }

  @PostMapping("/{id}/update")
  public String updateRestaurant(
      @PathVariable("id") ObjectId id,
      @Valid RestaurantForm restaurantForm,
      BindingResult bindingResult,
      RedirectAttributes redirect,
      Model model
  ) {
    if(bindingResult.hasErrors()) {
      model.addAttribute("add", false);
      return "pages/restaurants/add-edit";
    }

    try {
      restaurantsService.updateRestaurant(id, restaurantForm);
    } catch(IllegalStateException exception) {
      log.error("Cannot update restaurant with id {} because of error: {}", id, exception.getMessage());
      redirect.addFlashAttribute("creation_error", "Impossibile aggiornare l'attività, ci sono alcuni prodotti con le categorie appena modificate");
    } catch (Exception exception) {
      log.error("Cannot update restaurant with id {} because of error {}", id, exception.getMessage());
      redirect.addFlashAttribute("creation_error", "Impossibile aggiornare l'attività");
    }
    return "redirect:/restaurants/" + id.toString();
  }

  @GetMapping("/{id}/delete")
  public String deleteRestaurant(@PathVariable("id") ObjectId id, Model model) {
    restaurantsService.deleteRestaurant(id);
    return "redirect:/profile";
  }
}
