package com.ingsw.cfim.business_mvc.restaurants.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@NoArgsConstructor
public class WorkingTime {
  Boolean free = false;
  String opening;
  String closing;

  private Boolean isCorrect(String time) {
    Pattern pattern = Pattern.compile("^\\d{2}:\\d{2}$");
    Matcher matcher = pattern.matcher(time);
    return matcher.matches();
  }

  public void setOpening(String time) {
    if (isCorrect(time)) {
      opening = time;
    }
  }

  public void setClosing(String time) {
    if (isCorrect(time)) {
      closing = time;
    }
  }

}
