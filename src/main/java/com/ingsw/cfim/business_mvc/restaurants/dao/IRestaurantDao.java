package com.ingsw.cfim.business_mvc.restaurants.dao;

import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import org.bson.types.ObjectId;

import java.util.List;

public interface IRestaurantDao {
  public List<Restaurant> findAllRestaurantsForUser(ObjectId owner);
  public Restaurant find(ObjectId restaurantId, ObjectId ownerId);
  public void create(Restaurant restaurant);
  public void update(Restaurant restaurant);
  public void delete(Restaurant restaurant);
}
