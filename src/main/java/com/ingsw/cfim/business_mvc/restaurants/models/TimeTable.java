package com.ingsw.cfim.business_mvc.restaurants.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TimeTable {
  private WorkingTime monday;
  private WorkingTime tuesday;
  private WorkingTime wednesday;
  private WorkingTime thursday;
  private WorkingTime friday;
  private WorkingTime saturday;
  private WorkingTime sunday;
}
