package com.ingsw.cfim.business_mvc.restaurants.responses;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;

import java.util.Set;

public class RestaurantInfoExtended extends RestaurantInfo {
  private final Restaurant restaurant;

  public RestaurantInfoExtended(final Restaurant restaurant) {
    super(restaurant);
    this.restaurant = restaurant;
  }

  @JsonGetter("cuisine")
  public String getCuisine() {
    return restaurant.getCuisine();
  }

  @JsonGetter("categories")
  public Set<String> getFoodCategories() {
    return restaurant.getFoodCategories();
  }

  @JsonGetter("vatNumber")
  public String getVatNumber() {
    return restaurant.getVatNumber();
  }

}
