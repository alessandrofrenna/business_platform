package com.ingsw.cfim.business_mvc.restaurants.models;

import com.ingsw.cfim.business_mvc.common.models.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode(of = "id")
@Document(collection = "restaurants")
@CompoundIndex(name = "name_user", def = "{'name': 1, 'owner.id': 1, 'address.label': 1}", unique = true)
public class Restaurant {
  @MongoId
  private ObjectId id;

  private String name;
  private String cuisine;
  private String type;
  private String description;
  private Set<String> foodCategories;
  private String phoneNumber;
  private String vatNumber;
  private Address address;

  private TimeTable weeklyOpeningSchedule;

  @DBRef
  private User owner;

  @CreatedDate
  private Date createdAt;

  @LastModifiedDate
  private Date updatedAt;

  public Restaurant() {
    this.foodCategories = new HashSet<>();
  }

  @PersistenceConstructor
  public Restaurant(String name, String type, String phoneNumber, String vatNumber, Address address) {
    this();
    this.name = name;
    this.type = type;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.vatNumber = vatNumber;
  }

  public void addCategory(String category) {
    this.foodCategories.add(category);
  }
}
