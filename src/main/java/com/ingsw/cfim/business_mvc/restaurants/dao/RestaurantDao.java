package com.ingsw.cfim.business_mvc.restaurants.dao;

import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import com.ingsw.cfim.business_mvc.restaurants.repositories.RestaurantRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantDao implements IRestaurantDao {

  @Autowired
  private RestaurantRepository restaurants;

  @Override
  public List<Restaurant> findAllRestaurantsForUser(final ObjectId ownerId) {
    return restaurants.findByOwnerId(ownerId).orElse(null);
  }

  @Override
  public Restaurant find(final ObjectId restaurantId, final ObjectId ownerId) {
    return restaurants.findByIdAndOwnerId(restaurantId, ownerId).orElse(null);
  }

  @Override
  public void create(final Restaurant restaurant) {
    restaurants.insert(restaurant);
  }

  @Override
  public void update(final Restaurant restaurant) {
    restaurants.save(restaurant);
  }

  @Override
  public void delete(final Restaurant restaurant) {
    restaurants.delete(restaurant);
  }
}
