package com.ingsw.cfim.business_mvc.restaurants.repositories;

import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RestaurantRepository extends MongoRepository<Restaurant, ObjectId> {
  Optional<List<Restaurant>> findByOwnerId(ObjectId id);
  Optional<Restaurant> findByIdAndOwnerId(ObjectId restaurantId, ObjectId ownerId);

  @Query("{ 'address.location': { $near: ?0 } }")
  Optional<List<Restaurant>> findByAddressNear(GeoJsonPoint point);

  @Query("{ 'address.location': { $near: ?0, $maxDistance: ?1 } }")
  Optional<List<Restaurant>> findByAddressNear(GeoJsonPoint point, Double distance);
}
