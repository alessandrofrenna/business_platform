package com.ingsw.cfim.business_mvc.auth;

import com.ingsw.cfim.business_mvc.common.models.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class MongoUserDetails implements UserDetails {
  private static final long serialVersionUID = 4240846970536436441L;
  private User user;
  private List<GrantedAuthority> grantedAuthorities;

  public MongoUserDetails(User user, List<GrantedAuthority> authorities) {
    this.user = user;
    this.grantedAuthorities = authorities;
  }

  public String getFullName() {
    return this.user.getFullName();
  }

  public User getUser() {
    return this.user;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return grantedAuthorities;
  }

  @Override
  public String getPassword() {
    return this.user.getPassword();
  }

  @Override
  public String getUsername() {
    return this.user.getEmail();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return this.user.getEnabled();
  }
}
