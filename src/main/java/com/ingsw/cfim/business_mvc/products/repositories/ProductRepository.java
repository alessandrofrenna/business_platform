package com.ingsw.cfim.business_mvc.products.repositories;

import com.ingsw.cfim.business_mvc.products.models.Product;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProductRepository extends MongoRepository<Product, ObjectId> {
  Optional<Product> findByIdAndRestaurantId(ObjectId productId, ObjectId restaurantId);

  Optional<List<Product>> findByRestaurantId(ObjectId restaurantId);

  @Query(value = "{ 'category': { $in: ?0 } }")
  Optional<List<Product>> findInCategoryAndRestaurantId(Set<String> categories, ObjectId restaurantId);
}
