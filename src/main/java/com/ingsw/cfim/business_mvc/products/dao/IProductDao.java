package com.ingsw.cfim.business_mvc.products.dao;

import com.ingsw.cfim.business_mvc.products.models.Product;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Set;

public interface IProductDao {
  List<Product> findAllProductsForRestaurant(ObjectId restaurantId);
  List<Product> findAllProductsForRestaurant(ObjectId restaurantId, Set<String> categories);
  Product findProduct(ObjectId productId, ObjectId restaurantId);
  void createProduct(Product product);
  void updateProduct(Product product);
  void deleteProduct(Product product);
}
