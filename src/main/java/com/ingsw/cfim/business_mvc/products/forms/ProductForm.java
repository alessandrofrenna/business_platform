package com.ingsw.cfim.business_mvc.products.forms;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProductForm {
  @NotBlank(message = "Name must not be null and must be a valid string")
  String name;

  @NotBlank(message = "Category must not be null and a valid string")
  String category;

  @NotNull(message = "Ingredients must be a list of comma separated strings")
  List<String> ingredients;

  @NotNull(message = "Price must not be null and must be a valid number")
  @Min(value = 0, message = "Price must be at least 0 euros")
  Double price;
}
