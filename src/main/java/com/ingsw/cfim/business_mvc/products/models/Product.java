package com.ingsw.cfim.business_mvc.products.models;

import com.ingsw.cfim.business_mvc.restaurants.models.Restaurant;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import java.util.List;

@Data
@NoArgsConstructor
@Document(collection = "products")
@CompoundIndex(name="name_restaurant", def = "{'name': 1, 'restaurant._id': 1}", unique = true)
public class Product {
  @MongoId
  private ObjectId id;

  private String name;
  private String category;
  private List<String> ingredients;
  private Double price;

  @DBRef
  private Restaurant restaurant;

  @PersistenceConstructor
  public Product(String name, Double price) {
    this.name = name;
    this.price = price;
  }
}
