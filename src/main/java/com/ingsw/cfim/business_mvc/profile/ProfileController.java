package com.ingsw.cfim.business_mvc.profile;

import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.common.repositories.UserRepository;
import com.ingsw.cfim.business_mvc.profile.models.OwnerProfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class ProfileController {

  @Autowired
  private UserRepository repo;

  @GetMapping("/profile")
  public String showProfileInfo(Model model, Principal principal) {
    String email = principal.getName();
    User user = this.repo.findUserByEmail(email);

    model.addAttribute("profile", (OwnerProfile) user.getProfile());
    model.addAttribute("mail", email);
    return "pages/profile/index";
  }

  @GetMapping("/profile/edit")
  public String showProfileForm(OwnerProfile ownerProfile, Principal principal, Model model) {
    model.addAttribute("add", false);
    OwnerProfile profile = (OwnerProfile) this.repo.findUserByEmail(principal.getName()).getProfile();
    model.addAttribute("ownerProfile", profile);
    return "pages/profile/add-edit";
  }

  @PostMapping("/profile/edit")
  public String editProfileInfo(@Valid OwnerProfile ownerProfile, BindingResult binding, Principal principal,
      Model model) {
    if (binding.hasErrors()) {
      model.addAttribute("add", false);
      return "pages/profile/add-edit";
    }
    User user = this.repo.findUserByEmail(principal.getName());
    user.setProfile(ownerProfile);
    this.repo.save(user);
    return "redirect:/";
  }

}
