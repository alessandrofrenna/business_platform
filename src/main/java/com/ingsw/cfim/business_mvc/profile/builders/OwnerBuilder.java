package com.ingsw.cfim.business_mvc.profile.builders;

import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.common.models.Profile;
import com.ingsw.cfim.business_mvc.profile.models.OwnerProfile;
import com.ingsw.cfim.business_mvc.registration.IRegistrationService;

import com.ingsw.cfim.business_mvc.registration.forms.RegistrationForm;
import org.springframework.dao.DuplicateKeyException;

public class OwnerBuilder implements AccountBuilder {
  private IRegistrationService regSvc;

  private User toBeCreated;

  private OwnerBuilder(IRegistrationService svc) {
    this.regSvc = svc;
  }

  public static AccountBuilder init(IRegistrationService service) {
    return new OwnerBuilder(service);
  }

  @Override
  public AccountBuilder user(RegistrationForm userInput) {
    this.toBeCreated = new User();
    this.toBeCreated.setEmail(userInput.getEmail());
    this.toBeCreated.setPassword(userInput.getPassword());
    this.toBeCreated.setEnabled(true);
    return this;
  }

  @Override
  public AccountBuilder profile(Profile profileInput) {
    this.toBeCreated.setProfile((OwnerProfile) profileInput);
    return this;
  }

  @Override
  public User build() throws DuplicateKeyException {
    return this.regSvc.register(this.toBeCreated);
  }
}
