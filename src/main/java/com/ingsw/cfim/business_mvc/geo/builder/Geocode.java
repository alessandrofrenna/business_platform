package com.ingsw.cfim.business_mvc.geo.builder;

public interface Geocode {
  public Geocode setAPIEndpoint(String url);
  public Geocode setRequestParam(String key, String value);
  public String buildRequestUrl();
}
