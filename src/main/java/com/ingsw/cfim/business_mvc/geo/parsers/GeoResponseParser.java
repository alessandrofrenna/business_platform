package com.ingsw.cfim.business_mvc.geo.parsers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ingsw.cfim.business_mvc.restaurants.models.Address;
import org.json.JSONObject;

public interface GeoResponseParser {
  JSONObject partial = new JSONObject();

  GeoResponseParser parseAddress();
  GeoResponseParser parseLocation();
  Address getParsedAddress() throws JsonProcessingException;
}
