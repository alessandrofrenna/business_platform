package com.ingsw.cfim.business_mvc.geo.parsers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingsw.cfim.business_mvc.restaurants.models.Address;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

public final class AddressParser implements  GeoResponseParser {
  JSONObject partial;
  private JSONObject parsedAddress;
  private GeoJsonPoint parsedLocation;

  public AddressParser(JSONObject json) {
    JSONArray temp = json
        .getJSONObject("Response")
        .getJSONArray("View");

    if(!temp.isEmpty()) {
      this.partial = temp.getJSONObject(0)
          .getJSONArray("Result")
          .getJSONObject(0)
          .getJSONObject("Location");
    } else {
      this.partial = null;
    }

  }

  @Override
  public AddressParser parseAddress() {
    if(this.partial != null)
      this.parsedAddress = partial.getJSONObject("Address");
    return this;
  }

  @Override
  public AddressParser parseLocation() {
    if(this.partial != null) {
      this.partial = this.partial.getJSONObject("DisplayPosition");

      double longitude = this.partial.getDouble("Longitude");
      double latitude = this.partial.getDouble("Latitude");

      this.parsedLocation = new GeoJsonPoint(longitude, latitude);
    }

    return this;
  }

  @Override
  public Address getParsedAddress() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

    Address address = new Address();

    if(this.parsedAddress != null) {
      JsonNode node = mapper.readTree(this.parsedAddress.toString());


      address.setLabel(node.has("Label") ? node.get("Label").textValue() : null);
      address.setCountry(node.has("Country") ? node.get("Country").textValue() : null);
      address.setState(node.has("State") ? node.get("State").textValue() : null);
      address.setCounty(node.has("County") ? node.get("County").textValue() : null);
      address.setCity(node.has("City") ? node.get("City").textValue() : null);
      address.setDistrict(node.has("District") ? node.get("District").textValue() : null);
      address.setStreet(node.has("Street") ? node.get("Street").textValue() : null);
      address.setNumber(node.has("HouseNumber") ? node.get("HouseNumber").textValue() : null);
      address.setPostalCode(node.has("PostalCode") ? node.get("PostalCode").asInt() : null);
      address.setLocation(this.parsedLocation);
    }

    this.partial = null;
    this.parsedLocation = null;
    this.parsedAddress = null;

    return address;
  }
}
