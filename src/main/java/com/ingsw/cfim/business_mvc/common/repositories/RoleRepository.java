package com.ingsw.cfim.business_mvc.common.repositories;

import com.ingsw.cfim.business_mvc.common.models.Role;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface RoleRepository extends MongoRepository<Role, ObjectId> {
  public Role findByRole(String role);
}
