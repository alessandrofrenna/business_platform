package com.ingsw.cfim.business_mvc.common.controllers;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.slf4j.Logger;


@ControllerAdvice
public class NotFoundErrorController {

  private static Logger logger = LoggerFactory.getLogger(NotFoundErrorController.class);

  @ExceptionHandler(Throwable.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public String exception(final Throwable throwable, final Model model) {
    logger.error("Exception during execution of SpringSecurity application, not found path {}", throwable.getMessage());
    String errorMessage = throwable.getMessage();
    model.addAttribute("errorMessage", errorMessage);
    return "error";
  }

}
