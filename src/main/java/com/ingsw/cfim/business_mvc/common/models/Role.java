package com.ingsw.cfim.business_mvc.common.models;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "role")
@Document(collection = "roles")
public class Role {
  @MongoId
  private ObjectId id;

  @Indexed(name="role_index", unique = true, direction = IndexDirection.DESCENDING)
  private String role;

  @PersistenceConstructor
  public Role(String role) {
    this.role = role;
  }

}
