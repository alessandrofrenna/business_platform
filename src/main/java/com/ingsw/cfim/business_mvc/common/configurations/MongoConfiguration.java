package com.ingsw.cfim.business_mvc.common.configurations;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

@Configuration
@ConfigurationProperties(prefix="mongo")
public class MongoConfiguration extends AbstractMongoClientConfiguration {
    private String connection;
    private String db;

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getConnection() {
        return this.connection;
    }

    public String getDb() {
        return this.db;
    }

    @Override
    public String getDatabaseName() {
        return this.db;
    }

    @Override
    public MongoClient mongoClient() {
        return MongoClients.create(this.connection);
    }
}
