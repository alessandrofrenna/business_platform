package com.ingsw.cfim.business_mvc.common.controllers;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/")
    public String showHomepage() {
        Boolean isAuthenticated = SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
        Boolean isNotAnonymous = !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken);

        if(isAuthenticated && isNotAnonymous) return "redirect:/profile";

        return "pages/homepage/index";
    }
}
