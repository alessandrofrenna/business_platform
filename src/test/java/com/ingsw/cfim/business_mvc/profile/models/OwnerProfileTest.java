package com.ingsw.cfim.business_mvc.profile.models;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class OwnerProfileTest {

  private Pattern pattern;

  private OwnerProfile profile = new OwnerProfile();

  private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);

  @Before()
  public void setup() throws ParseException {
    String regex = "^[A-Z]{6}[0-9]{2}[A|B|C|D|E|H|L|M|P|R|S|T][0-9]{2}\\w{4}[A-Z]$";
    this.pattern = Pattern.compile(regex);
    this.profile.setName("Mario");
    this.profile.setLastName("Rossi");
    this.profile.setDateOfBirth(this.formatter.parse("1982-08-11"));
    this.profile.setCountry("Italy");
    this.profile.setCity("Rome");
    this.profile.setFiscalNumber("RSSMRA82H11C892D");
  }

  @Test
  public void verifyThatProfileHasCorrectParameters() {
    assertEquals(String.class, this.profile.getName().getClass());
    assertEquals(String.class, this.profile.getLastName().getClass());
    assertEquals(String.class, this.profile.getCountry().getClass());
    assertEquals(String.class, this.profile.getCity().getClass());
    assertEquals(Date.class, this.profile.getDateOfBirth().getClass());
    assertEquals(String.class, this.profile.friendlyDob().getClass());
    assertEquals(String.class, this.profile.getFiscalNumber().getClass());
  }

  @Test
  public void fiscalCodeMatchingRegexShouldBeTrue() {
    assertTrue(pattern.matcher(this.profile.getFiscalNumber()).matches());
  }

  @Test
  public void fiscalCodeMatchingRegexShouldBeFalse() {
    this.profile.setFiscalNumber("HELLO");
    assertFalse(pattern.matcher(this.profile.getFiscalNumber()).matches());
    assertTrue(this.profile.getFiscalNumber().length() < 16);

    this.profile.setFiscalNumber("RSSMRA82O11C892D");
    assertFalse(pattern.matcher(this.profile.getFiscalNumber()).matches());

    this.profile.setFiscalNumber("RSSMRA82H11C8923FWD");
    assertFalse(pattern.matcher(this.profile.getFiscalNumber()).matches());
    assertTrue(this.profile.getFiscalNumber().length() > 16);
  }

}
