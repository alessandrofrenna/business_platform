package com.ingsw.cfim.business_mvc.integration;

import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.common.configurations.MongoConfiguration;
import com.ingsw.cfim.business_mvc.common.models.Role;
import com.ingsw.cfim.business_mvc.common.repositories.RoleRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@Import(MongoConfiguration.class)
public class RoleRepositoryTest {

  @Autowired
  private RoleRepository roleRepo;

  @Before
  public void setup() {
    roleRepo.save(new Role("tester"));
  }

  @Test
  public void existingRole() {
    assertNotNull(roleRepo.findByRole("tester"));
  }

  public void nonExistingRole() {
    assertNull(roleRepo.findByRole("master"));
  }

  @After
  public void destroy() {
    this.roleRepo.deleteAll();
  }
}