package com.ingsw.cfim.business_mvc.geo;

import static org.junit.Assert.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ingsw.cfim.business_mvc.restaurants.models.Address;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GetCoordinatesFromAddressTest {

  @Autowired
  GeocodeRequest requestProvider;

  @Test
  public void gettingJSONCoordinates() throws JSONException, JsonProcessingException {
    Address address = requestProvider.getAddressFromAPI("Viale+del+Fante+52,+Altofonte");
    assertNotNull(address);
  }

  @Test
  public void gettingJSONCoordinatesMustReturnNull() throws JSONException, JsonProcessingException {
    Address address = requestProvider.getAddressFromAPI(null);
    assertNull(address);
  }

  @Test
  public void gettingJSONCoordinatesMustReturnEmptyAddressObject() throws JSONException, JsonProcessingException {
    Address address = requestProvider.getAddressFromAPI("asl3209qdwlqsasaa");
    assertNull(address.getLabel());
    assertNull(address.getCity());
    assertNull(address.getCountry());
    assertNull(address.getCounty());
    assertNull(address.getDistrict());
    assertNull(address.getLocation());
    assertNull(address.getNumber());
    assertNull(address.getPostalCode());
    assertNull(address.getState());
    assertNull(address.getStreet());
  }
}
