package com.ingsw.cfim.business_mvc.auth;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PasswordServiceTest {
  private PasswordService pwds;

  private String hash;

  @Before
  public void setup() {
    this.pwds = new PasswordService();
    this.hash = this.pwds.encode("password for testing purpose");
  }

  @Test
  public void hashIsAString() {
    assertTrue(this.hash instanceof String);
  }

  @Test
  public void hashesForSamePasswordShouldBeDifferent() {
    String first = this.pwds.encode("the hashes must be different");
    String second = this.pwds.encode("the hashes must be different");
    assertNotEquals(first, second);
  }

  @Test
  public void hashVerificationIsTrue() {
    assertTrue(this.pwds.matches("password for testing purpose", this.hash));
  }
}
