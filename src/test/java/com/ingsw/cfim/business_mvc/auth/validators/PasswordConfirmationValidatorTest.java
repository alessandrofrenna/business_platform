package com.ingsw.cfim.business_mvc.auth.validators;

import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.registration.forms.RegistrationForm;
import org.junit.Test;

public class PasswordConfirmationValidatorTest {

  private PasswordConfirmationConstraint pwc = new PasswordConfirmationConstraint();

  @Test
  public void testPasswordEqualityShouldReturnTrue() {
    RegistrationForm form = new RegistrationForm();
    form.setEmail("test@example.org");
    form.setPassword("test password");
    form.setPasswordConfirmation("test password");

    assertTrue(this.pwc.isValid(form, null));
  }

  @Test
  public void testPasswordEqualityShouldReturnFalse() {
    RegistrationForm form = new RegistrationForm();
    form.setEmail("test@example.org");
    form.setPassword("test password");
    form.setPasswordConfirmation("test wrong password");

    assertFalse(this.pwc.isValid(form, null));
  }
}
