package com.ingsw.cfim.business_mvc.auth;

import static org.junit.Assert.*;

import com.ingsw.cfim.business_mvc.common.models.Role;
import com.ingsw.cfim.business_mvc.common.models.User;
import com.ingsw.cfim.business_mvc.common.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {
  @Mock
  private UserRepository userRepo;

  @Mock
  private CurrentLoggedIn current;

  @InjectMocks
  private AuthService authService;

  private Role role;

  private User user;

  private List<GrantedAuthority> authorities = new ArrayList<>();

  private UserDetails details;

  @Before
  public void setup() {
    this.role = new Role("restaurant_owner");
    this.user = new User("example@gmail.com", "test pwd hashed");
    this.user.setEnabled(true);
    this.user.setRoles(this.role);

    this.user.getRoles().forEach((role) -> {
      this.authorities.add(new SimpleGrantedAuthority(role.getRole()));
    });

    this.details = new MongoUserDetails(this.user, this.authorities);
  }

  @Test()
  public void loadByUsernameSuccess() {
    Mockito.when(this.userRepo.findUserByEmail(Mockito.anyString())).thenReturn(this.user);

    MongoUserDetails created = (MongoUserDetails) this.authService.loadUserByUsername("example@gmail.it");

    assertEquals(this.details.getUsername(), created.getUsername());
    assertEquals(this.details.getPassword(), created.getPassword());
  }

  @Test(expected = UsernameNotFoundException.class)
  public void loadByUsernameFail() {
    Mockito.when(this.userRepo.findUserByEmail(Mockito.anyString())).thenReturn(null);

    this.authService.loadUserByUsername("example@gmail.it");
  }

}
