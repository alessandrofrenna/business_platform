package com.ingsw.cfim.business_mvc.registration.forms;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class RegistrationFormTest {
  private static Validator validator;

  @BeforeClass
  public static void setUp() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void emailIsNull() {
    RegistrationForm user = new RegistrationForm();
    user.setPassword("for testing purpose");
    user.setPasswordConfirmation("for testing purpose");

    Set<ConstraintViolation<RegistrationForm>> constraintViolations = validator.validate(user);

    assertEquals(1, constraintViolations.size());
    assertEquals("Email must not be blank", constraintViolations.iterator().next().getMessage());
  }

  @Test
  public void passwordIsNull() {
    RegistrationForm user = new RegistrationForm();
    user.setEmail("alessandrofrenna95@gmail.com");

    Set<ConstraintViolation<RegistrationForm>> constraintViolations = validator.validate(user);

    assertEquals(1, constraintViolations.size());
    assertEquals("Password must not be blank", constraintViolations.iterator().next().getMessage());
  }

  @Test
  public void passwordConfirmationIsNull() {
    RegistrationForm user = new RegistrationForm();
    user.setEmail("alessandrofrenna95@gmail.com");
    user.setPassword("testing only");

    Set<ConstraintViolation<RegistrationForm>> constraintViolations = validator.validate(user);

    assertEquals(1, constraintViolations.size());
    assertEquals("Password Confirmation must be equals to password",
        constraintViolations.iterator().next().getMessage());
  }

  @Test
  public void invalidEmail() {
    RegistrationForm user = new RegistrationForm();
    user.setEmail("alessandrofrenna95");
    user.setPassword("testing only");
    user.setPasswordConfirmation("testing only");

    Set<ConstraintViolation<RegistrationForm>> constraintViolations = validator.validate(user);

    assertEquals(1, constraintViolations.size());
    assertEquals("Email must be a valid email", constraintViolations.iterator().next().getMessage());
  }

  @Test
  public void passwordConfirmationPresentButPasswordIsNull() {
    RegistrationForm user = new RegistrationForm();
    user.setEmail("alessandrofrenna95@gmail.com");
    user.setPasswordConfirmation("testing only");

    Set<ConstraintViolation<RegistrationForm>> constraintViolations = validator.validate(user);

    Object[] errorsArray = constraintViolations.toArray();

    assertEquals(2, errorsArray.length);
  }

  @Test
  public void validRegistrationForm() {
    RegistrationForm user = new RegistrationForm();
    user.setEmail("alessandrofrenna95@gmail.com");
    user.setPassword("testing only");
    user.setPasswordConfirmation("testing only");

    Set<ConstraintViolation<RegistrationForm>> constraintViolations = validator.validate(user);

    assertEquals(0, constraintViolations.size());
  }

}
