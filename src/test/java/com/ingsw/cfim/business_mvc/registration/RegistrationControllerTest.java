package com.ingsw.cfim.business_mvc.registration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.ingsw.cfim.business_mvc.registration.forms.RegistrationForm;
import org.junit.Test;


import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RegistrationControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private RegistrationService regSvc;

  @Test
  public void testForRegistrationContent() throws Exception {
    mockMvc.perform(
        get("/registration")
        .contentType("text/html")
    )
        .andExpect(view().name("pages/registration/index"));
  }

  @Test
  public void testForProfileContentErrorWithNoAuth() throws Exception {
    mockMvc.perform(
        get("/registration/profile")
    )
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testingForRegistrationFormCompilationCorrectness() throws Exception {
    mockMvc.perform(
        post("/registration")
            .param("email", "testing@gmail.com")
            .param("password", "testing password")
            .param("passwordConfirmation", "testing password")
    ).andExpect(model().hasNoErrors());
  }

  @Test
  public void testingForRegistrationFormError() throws Exception {
    mockMvc.perform(
        post("/registration")
            .param("email", "test")
            .param("password", "testing password")
            .param("passwordConfirmation", "testing")
    ).andExpect(model().hasErrors());
  }

  @Test
  public void testingForProfileAddPageCorrectDisplay() throws Exception {
    RegistrationForm user = new RegistrationForm();
    user.setEmail("example@test.it");
    user.setPassword("test password");
    user.setPasswordConfirmation("test password");

    MockHttpSession session = new MockHttpSession();

    session.setAttribute("UserForm", user);

    mockMvc.perform(
        get("/registration/profile")
          .session(session)
    ).andExpect(view().name("pages/profile/add-edit"));
  }

  @Test
  public void testingForProfileAddPageCorrectResult() throws Exception {
    RegistrationForm user = new RegistrationForm();
    user.setEmail("example@test.it");
    user.setPassword("test password");
    user.setPasswordConfirmation("test password");

    MockHttpSession session = new MockHttpSession();

    session.setAttribute("UserForm", user);

    mockMvc.perform(
        post("/registration/complete")
            .session(session)
            .param("name", "Alessandro")
            .param("lastName", "Frenna")
            .param("dateOfBirth", "1995-12-21")
            .param("country", "Italy")
            .param("city", "Palermo")
            .param("fiscalNumber", "FRNLSN95T21G273X")
    ).andExpect(model().hasNoErrors());
  }


  @Test
  public void testingForProfileAddPageErrors() throws Exception {
    RegistrationForm user = new RegistrationForm();
    user.setEmail("example@test.it");
    user.setPassword("test password");
    user.setPasswordConfirmation("test password");

    MockHttpSession session = new MockHttpSession();

    session.setAttribute("UserForm", user);

    mockMvc.perform(
        post("/registration/complete")
            .session(session)
            .param("name", "")
            .param("lastName", "")
            .param("dateOfBirth", "")
            .param("country", "")
            .param("city", "")
            .param("fiscalNumber", "FRNLSN95T21G273XK")
    ).andExpect(model().errorCount(7));
  }
}
